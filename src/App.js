import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import { incrementar, decrementar, setear } from "./reducers";

class App extends Component {
  handleSetear = () => {
    const { setear } = this.props;
    const { valor } = this.state;
    setear(Number(valor));
  };
  handleChange = (e) => {
    console.log("e", e);
    const { name, value } = e.target;
    console.log("name:", name, "value:", value);
    this.setState({
      [name]: value,
    });
  };
  render() {
    const { incrementar, decrementar, valor } = this.props;
    console.log(this.state);
    return (
      <div className="App">
        <p>{valor}</p>
        <button onClick={incrementar}>Incrementar</button>
        <button onClick={decrementar}>Decrementar</button>
        <input name="valor" onChange={this.handleChange}></input>
        <button onClick={this.handleSetear}>Setear</button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log({ state });
  return {
    valor: state,
  };
};
const mapDispatchToProps = (dispatch) => ({
  incrementar: () => dispatch(incrementar()),
  decrementar: () => dispatch(decrementar()),
  setear: (payload) => dispatch(setear(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);

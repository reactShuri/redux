// ACTION TYPES: Strings que definen la acción a ejecutar (UNA ESPECIE DE CONTROLADOR)
const INCREMENTAR = "CONTADOR/INCREMENTAR";
const DECREMENTAR = "CONTADOR/DECREMENTAR";
const SETEAR = "CONTADOR/SETEAR";

// ACTION CREATORS: funciones que van a constriur un objeto con el formato que nos acepta redux para poder despachar las acciones
export const incrementar = () => ({
  type: INCREMENTAR,
});
export const decrementar = () => ({
  type: DECREMENTAR,
});
// La forma en que a estas acciones le podemos agregar datos es a traves del atributo payload
export const setear = (payload) => ({
  type: SETEAR,
  payload,
});

const initialState = 0;
// reducers tienen que retornar estados inmutables
// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
  //   console.log(action);
  switch (action.type) {
    case INCREMENTAR:
      return state + 1;
    case DECREMENTAR:
      return state - 1;
    case SETEAR:
      return action.payload;
    default:
      return state;
  }
}
